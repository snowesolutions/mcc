The goal of this exercise is to create a minimal mentions feed.  
You will be evaluated on your abilities to integrate (pixel perfect) the template and to use a json.

### Objectives:
 * Parse a JSON OR make an API CALL
 * Display the results respecting the template provided

### In-depth:

##### 1) FEED INTEGRATION:

You'll integrate the template template.sketch 

	-> HEADER

	The header need to be fix at the top.

	-> Feed

		- The width and height of each feed item are fix (width : 880px / Height : 110px)
		- The URL is a link . When you click on it you should open a new page. The displayed URL is in author_influence -> url , but it links to "original_url".
		- The title and Description have a max-width of 620px. The title need to be on one line. The Description has a max-height of 37px. If the description is too long you have to truncate string with ellipsis.

##### 2) PARSE JSON: 

You'll find an JSON extract from the mention API.
You have to write a function which parse the file feed.json (using the technology of your choice):  
`/json/feed.json`

you will dynamize the display of your integration by displaying the mentions collected in the json.

		- In the JSON file, you should use "description"
		- You should format the date from json "published_at" to a string like this example 19 sept (tips : https://momentjs.com/)
		- by default the logo is the mention logo. If there is a picture image in the json, this picture will be the logo


##### OR ) API CALL: 
Write an API Call (using the technology of your choice):  
`https://web.mention.net/api/accounts/{account_id}/alerts/{alert_id}/mentions`

Parameters: 
 * `account_id = 661072_53ca2jsh01c88c4wwkc0wockckk0w4440o4o0w8wkkgco4o888`  
 * `alert_id = 1214654`  
 
Query string:
 * `access_token = ZDdmNDVmYzU1NWZkMDkwMDc4YjBjMzYyZDk2MDI3NGVlNmFmNTJkZDU5MzBhYWRiZGZmNzAxOGM1NDkzNDYxYQ`
 
Warning: access_token must be in the query string.  
More information here:
 * [Doc Authentification](https://dev.mention.com/current/src/index.html#in-the-query-string) 
 * [Doc Endpoint](https://dev.mention.com/current/src/account/alert/mention/GetMentions.html)


> Nb: You will have to desactivate Same Domain Origin Policy on your browser:  
> Firefox: https://addons.mozilla.org/fr/firefox/addon/cors-everywhere/  
> Chrome: http://stackoverflow.com/questions/3102819/disable-same-origin-policy-in-chrome


### Ultimate Goal:
![objectif](http://i.imgur.com/GVPappy.jpg)


GLHF
